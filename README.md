# Personal Website #
![WithAssets.jpg](https://bitbucket.org/repo/5KMB7K/images/288908944-WithAssets.jpg)
*A personal project written in HTML, CSS, Javascript and PHP by Edmund Lewry 2017*

My first real attempt at working with Web Technologies, after spending much of my professional and academic careers working with application development. Using David Carr's tutorial - [Creating a blog from scratch with PHP](https://daveismyname.blog/creating-a-blog-from-scratch-with-php) as a base, I then extended the implementation with an html page, css and javascript to attempt to build a series of features that I thought would make a good introduction to web languages. I learnt a lot during the project and intend to produce a second version using what I've learnt.

### How do I get set up? ###

* You'll need PHP Installed and a server running which supports it
* The Blog elements run using a MySQL database, which I can provide
* Pull down the code and open up the index.html page

I had intended to host it, but decided I wanted to rewrite the project using some javascript frameworks instead of the PHP. So I'll wait to host it.

ClearBlueSkyBox.com coming soon!