<?php
	require('includes/config.php');

	$statement = $database->prepare('SELECT postID, postTitle, postCont, postDate FROM ta_blog_posts WHERE postID = :postID');
	$statement->execute(array(':postID' => $_GET['id']));
	$row = $statement->fetch();

	if($row['postID'] == '')
	{
		header('Location: ./');
		exit;
	}
?>

<!DOCTYPE html>
	<html lang='en'>
	<head>
		<meta charset="utf-8">
		<title>Blog - <?php echo $row['postTitle'];?></title>
		<link rel="stylesheet" type="text/css" href="stylesheets/core.css">
		<link rel="stylesheet" type="text/css" href="stylesheets/blogIndex.css">
		<link rel="stylesheet" type="text/css" href="stylesheets/blogPost.css">
	</head>
	
	<body>
		<div class="titleBanner">
			<h1>Edmund Lewry - Blog</h1>
		</div>

		<ul id="navBar">
			<li><a href="index.html#homeContent">HOME</a></li>
			<li><a href="index.html#projectContent">PROJECTS</a></li>
			<li><a href="blog_index.php">BLOG</a></li>
			<li><a href="index.html#contactContent">CONTACT</a></li>
		</ul>
		<div id="wrapper">
			<?php
				echo '<div>';
			    	echo '<h1>'.$row['postTitle'].'</h1>';
			    	echo '<p id="postedStat">Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).'</p>';
			    	echo '<div id="blogCont">';
			    		echo '<p>'.$row['postCont'].'</p>';
			    	echo '</div>';
				echo '</div>';
			?>

		</div>
	</body>
</html>