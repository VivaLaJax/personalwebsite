<?php
ob_start();
session_start();

//database credentials
define('DBHOST','localhost');
define('DBUSER','root');
define('DBPASS','');
define('DBNAME','personal_site');

$database = new PDO("mysql:host=".DBHOST.";port=3306;dbname=".DBNAME, DBUSER, DBPASS);
$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//set timezone
date_default_timezone_set('Europe/London');

function __autoload($class)
{
	$class = strtolower($class);

	$classpath = 'classes/class.'.$class . '.php';
	if(file_exists($classpath))
	{
		require_once $classpath;
	}

	$classpath = '../classes/class.'.$class . '.php';
	if(file_exists($classpath))
	{
		require_once $classpath;
	}

	//if call from within admin adjust the path
	$classpath = '../../classes/class.'.$class . '.php';
	if ( file_exists($classpath)) 
	{
	require_once $classpath;
	}  
}

$user = new User($database);
?>