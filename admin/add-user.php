
<?php
	require_once('../includes/config.php');

	if(!$user->is_logged_in())
	{
		header('Location: login.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Admin - Add User</title>
	</head>
	<body>
		<div id="wrapper">
			<?php include('menu.php'); ?>
			<p>
				<a href="users.php">User Admin Index</a>
			</p>

			<h2>Add User</h2>

			<?php
				if(isset($_POST['submit']))
				{
					extract($_POST);

					if($username=='')
					{
						$error[] = 'Please enter the username.';
					}

					if($password=='')
					{
						$error[] = 'Please enter the password.';
					}

					if($passwordConfirm=='')
					{
						$error[] = 'Please confirm the password.';
					}

					if($email=='')
					{
						$error[] = 'Please enter the email.';
					}

					if(!isset($error))
					{
						$hashedpassword = $user->create_hash($password);

						try
						{
							//insert into database
							$statement = $database->prepare('INSERT INTO ta_blog_members (username,password,email) VALUES (:username,:password,:email)');
							$statement->execute(array(
								':username' => $username,
								':password' => $hashedpassword,
								':email' => $email
								));

							header('Location: users.php?action=added');
							exit;
						} 
						catch (PDOException $e) 
						{
							echo $e->getMessage();
						}
					}
				}

				if(isset($error))
				{
					foreach ($error as $error) 
					{
						echo '<p class="error">'.$error.'</p>';
					}
				}
			?>

			<form action="" method="post">
				<p>
					<label>Username</label>
					<br />
					<input type="text" name="username" value='<?php if(isset($error)){ echo $_POST['username'];}?>'>
				</p>

				<p>
					<label>Password</label>
					<br />
			    	<input type='password' name='password' value='<?php if(isset($error)){ echo $_POST['password'];}?>'>
			    </p>

			    <p>
			    	<label>Confirm Password</label>
			    	<br />
			    	<input type='password' name='passwordConfirm' value='<?php if(isset($error)){ echo $_POST['passwordConfirm'];}?>'>
			    </p>

			    <p>
			    	<label>Email</label>
			    	<br />
			    	<input type='text' name='email' value='<?php if(isset($error)){ echo $_POST['email'];}?>'>
			    </p>
			    
			    <p>
			    	<input type='submit' name='submit' value='Add User'>
			    </p>
			</form>
		</div>
	</body>
</html>