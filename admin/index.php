<?php	
	//include config
	require_once('../includes/config.php');

	//if not logged in redirect to login
	if(!$user->is_logged_in())
	{
		header('Location: login.php');
	}

	if(isset($_GET['delpost']))
	{ 

    	$statement = $database->prepare('DELETE FROM ta_blog_posts WHERE postID = :postID') ;
    	$statement->execute(array(':postID' => $_GET['delpost']));

    	header('Location: index.php?action=deleted');
    	exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<script language="JavaScript" type="text/javascript">
		function delpost(id, title)
		{
		  if (confirm("Are you sure you want to delete '" + title + "'"))
		  {
		      window.location.href = 'index.php?delpost=' + id;
		  }
		}
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include('menu.php');?>
			<?php
				if(isset($_GET['action']))
				{ 
			    	echo '<h3>Post '.$_GET['action'].'.</h3>'; 
				}
			?>
				
			<table>
				<tr>
					<th>Title</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
				<?php
					try 
					{
						$statement = $database->query('SELECT postID, postTitle, postDate FROM ta_blog_posts ORDER BY postID DESC');
						while($row = $statement->fetch())
						{
							echo '<tr>';
							echo '<td>'.$row['postTitle'].'</td>';
							echo '<td>'.date('jS M Y',strtotime($row['postDate'])).'</td>';
							?>

							<td>
								<a href="edit-post.php?id=<?php echo $row['postID'];?>">Edit</a> |
								<a href="javascript:delpost('<?php echo $row['postID'];?>','<?php echo $row['postTitle'];?>')">Delete</a>
							</td>

							<?php
							echo '</tr>';
						}
					} 
					catch (PDOException $e) 
					{
						echo $e->getMessage();
					}
				?>
			</table>

			<p>
				<a href="add-post.php">Add Post</a>
			</p>
		</div>
	</body>
</html>