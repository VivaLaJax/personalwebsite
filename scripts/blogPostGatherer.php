<?php require('../includes/config.php') ?>
<?php

try
{
	$objArray = new stdClass();
	$objArray->data = array();

	$statement = $database->query('SELECT postID, postTitle, postDesc, postDate FROM ta_blog_posts ORDER BY postID DESC');
	while($row = $statement->fetch())
	{
		$obj = new stdClass();
		$obj->blogId = $row['postID'];
		$obj->blogTitle = $row['postTitle'];
		$obj->blogDescription = $row['postDesc'];
		$obj->blogDate = $row['postDate'];
		array_push($objArray->data, $obj);
	}
	echo json_encode($objArray);
}
catch(PDOException $exception)
{
	echo $exception->getMessage();
}

?>