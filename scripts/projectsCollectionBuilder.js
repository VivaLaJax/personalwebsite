/*
Find the files in the projects folder, server side.
From each one, pull out a title, a small image filepath and
a the file path to the page

Find the projects collection
For each page, add a link element, containing an image and a p element

Can work out how to do fancy layouts later
*/
function onGatherProjects()
{
	try
	{
		var oReq = new XMLHttpRequest(); //New request object
	    oReq.onreadystatechange = function() 
	    {
	    	if(this.readyState==4 && this.status==200)
	    	{
	    		onPHPGathererLoaded(this.responseText);
	    	}
	    };
	    oReq.open("GET", "scripts/projectFileGatherer.php", true);
	    //                               ^ Don't block the rest of the execution.
	    //                                 Don't wait until the request finishes to 
	    //                                 continue.
	    oReq.send();
	}
	catch(err)
	{
		alert(err.message);
	}
}

function onPHPGathererLoaded(text)
{
	//alert(text);
	var projectObjJSON = JSON.parse(text);

	var projects = projectObjJSON.data;
	var projectsCollectionElement = document.getElementsByClassName("projectsCollection")[0];

	var width = (screen.width*0.45)/projects.length;
	for (var i = 0; i < projects.length; i++)
	{
		projectsCollectionElement.innerHTML+=
			"<a href=\""+projects[i].pageHref+
			  "\"><img src=\""+projects[i].imageFilePath+
			   "\" width=\""+width+"\" height=\"100\" border=\"\"><p class=\"projectLinkMouseOverText\">"+projects[i].title+
			   "</p></a>";
	}
}