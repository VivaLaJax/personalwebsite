<?php

function pullTitleFromPage($file)
{
	$dom = new DOMDocument();
	$dom->loadHTML($file);

	$titleNode = $dom->getElementById("projectTitle");
	return $titleNode->nodeValue;
}

function pullImageFromPage($file)
{
	$dom = new DOMDocument();
	$dom->loadHTML($file);

	$imageNode = $dom->getElementById("projectTeaserImage");
	return $imageNode->getAttribute("src");
}

$dir = "..\projects";

//echo "Opening directories:\n";
// Open a directory, and read its contents
if (is_dir($dir))
{
	if ($dh = opendir($dir))
	{
		//echo "Attempting to read files in projects 
		$objArray = new stdClass();
		$objArray->data = array();
		while (($file = readdir($dh)) !== false)
		{
			if(strpos($file, ".html"))
			{
				$rawData = file_get_contents("..\\projects\\" . $file);
				$obj = new stdClass();
				$obj->title = pullTitleFromPage($rawData);
				$obj->imageFilePath = pullImageFromPage($rawData);
				$obj->pageHref = "projects\\" . $file;
				array_push($objArray->data, $obj);
			}
		}
		echo json_encode($objArray);
    	closedir($dh);
	}
	else
	{
		echo "Error opening prjects directory";
	}
}
else
{
	echo "Projects directory not found";
}
?>