<?php
try
{
	$statement = $database->query('SELECT postID, postTitle, postDesc, postDate FROM ta_blog_posts ORDER BY postID DESC');
	while($row = $statement->fetch())
	{
		echo '<div>';
			echo '<h1><a href="viewpost.php?id='.$row['postID'].'">'.$row['postTitle'].'</a></h1>';
			echo '<p>Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).'</p>';
            echo '<p>'.$row['postDesc'].'</p>';                
            echo '<p><a href="viewpost.php?id='.$row['postID'].'">Read More</a></p>';
		echo '</div>';
	}
}
catch(PDOException $exception)
{
	echo $exception->getMessage();
}
?>