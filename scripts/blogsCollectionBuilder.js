/*
	<div class="blogTeaserCollection">
		<div class="blogTeaser">
			<a class="blogTitle" href="">Demo Blog</a>
			<p class="blogTease">This here is a demo blog. It's tough time being a demo blog. I myself wish I knew how to blah blah blah blog.</p>
			<div class="continueLink">
				<a href="">Continue...</a>
			</div>
		</div>
	</div>

*/

function onGatherBlogTeasers()
{
	var oReq = new XMLHttpRequest(); //New request object
    oReq.onreadystatechange = function() 
    {
    	if(this.readyState==4 && this.status==200)
    	{
    		onPHPBlogDBLoaded(this.responseText);
    	}
    };
    oReq.open("GET", "scripts/blogPostGatherer.php", true);
    //                               ^ Don't block the rest of the execution.
    //                                 Don't wait until the request finishes to 
    //                                 continue.
    oReq.send();
}

function onPHPBlogDBLoaded(text)
{
	//alert(text);
	var blogsObjJSON = JSON.parse(text);
	var blogs = blogsObjJSON.data;

	var blogTeaserCollectionElement = document.getElementsByClassName("blogTeaserCollection")[0];

	for (var i = 0; i < blogs.length && i<3; i++)
	{
		blogTeaserCollectionElement.innerHTML+=
			"<div class=\"blogTeaser\">"+
				"<a class=\"blogTitle\" href=\"viewpost.php?id="+blogs[i].blogId+"\">"+blogs[i].blogTitle+"</a>"+
				"<p class=\"blogDate\">"+blogs[i].blogDate+"</p>"+
				"<p class=\"blogTease\">"+blogs[i].blogDescription+"</p>"+
				"<div class=\"continueLink\">"+
					"<a href=\"viewpost.php?id="+blogs[i].blogId+"\">Continue...</a>"+
				"<\/div>"+
			"<\/div>";
	}
}