<?php
class User
{
	private $database;

	public function __construct($database)
	{
		$this->database = $database;
	}

	public function is_logged_in()
	{
		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
		{
			return true;
		}
	}

	public function create_hash($value)
	{
		return $hash = crypt($value, '$2a$12'.substr(str_replace('+', '.', base64_decode(sha1(microtime(true),true))), 0, 22));
	}

	public function login($username,$password)
	{
		$hashed = $this->get_user_hash($username);
		if($this->password_verify($password,$hashed) == 1)
		{
			$_SESSION['loggedin'] = true;
			return true;
		}
	}

	public function logout()
	{
		session_destroy();
	}

	private function get_user_hash($username)
	{
		try 
		{
			$statement = $this->database->prepare('SELECT password FROM ta_blog_members WHERE username = :username');
			$statement->execute(array('username' => $username));

			$row = $statement->fetch();
			return $row['password'];
		}
		catch (PDOException $e) 
		{
			echo '<p class="error">'.$e->getMessage().'</p>';
		}
	}

	private function password_verify($password,$hash)
	{
		return $hash == crypt($password, $hash);
	}

/*	public function addUserOverride($username,$password)
	{
		$hashedpassword = $this->create_hash($password);
		$email = "whatever@whatever";
		try
		{
			//insert into database
			$statement = $this->database->prepare('INSERT INTO ta_blog_members (username,password,email) VALUES (:username,:password,:email)');
			$statement->execute(array(
				':username' => $username,
				':password' => $hashedpassword,
				':email' => $email
				));

			exit;
		} 
		catch (PDOException $e) 
		{
			echo $e->getMessage();
		}
	}
*/
}
?>