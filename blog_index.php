<?php require('includes/config.php') ?>
<!DOCTYPE html>
	<html lang='en'>
	<head>
		<meta charset="utf-8">
		<title>Blog</title>
		<link rel="stylesheet" type="text/css" href="stylesheets/core.css">
		<link rel="stylesheet" type="text/css" href="stylesheets/blogIndex.css">
	</head>
	
	<body>
		<div class="titleBanner">
			<h1>Edmund Lewry - Blog</h1>
		</div>

		<ul id="navBar">
			<li><a href="index.html#homeContent">HOME</a></li>
			<li><a href="index.html#projectContent">PROJECTS</a></li>
			<li><a href="blog_index.php">BLOG</a></li>
			<li><a href="index.html#contactContent">CONTACT</a></li>
		</ul>
		<div id="wrapper">			
			<div id="blogs">
				<!-- Dynamically filled -->
			</div>

			<!--<h1 id="postsTitle">POSTS</h1>-->
			<?php
				try
				{
					$statement = $database->query('SELECT postID, postTitle, postDesc, postDate FROM ta_blog_posts ORDER BY postID DESC');
					while($row = $statement->fetch())
					{
						echo '<div id="item">';
							echo '<div id="itemContent">';
								echo '<h2><a href="viewpost.php?id='.$row['postID'].'">'.$row['postTitle'].'</a></h1>';
								echo '<p id="postedStat">Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).'</p>';
				                echo '<p>'.$row['postDesc'].'</p>';                
				                echo '<p><a href="viewpost.php?id='.$row['postID'].'">Read More</a></p>';
				            echo '</div>';
						echo '</div>';
					}
				}
				catch(PDOException $exception)
				{
					echo $exception->getMessage();
				}
			?>
		</div>
	</body>
</html>